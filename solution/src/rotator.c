#include "rotator.h"
#include <malloc.h>

void movePixel(int x, int y, Image* new, Image* src){
    new->colors[x][ y] = src->colors[src->height - y - 1][x];
}
void freeImage(Image* ima){
    for (int i = 0; i < ima->height; ++i) {
        free(ima->colors[i]);
    }
    free(ima->colors);
    free(ima);
}
Image* rotate(Image* const src){
    long long width = src->width;
    long long height = src->height;

    Image* new = malloc(sizeof(struct Image));
    new->width = height;
    new->height = width;

    new->colors = malloc(sizeof(Color*) * width);
    for (int i = 0; i < width; ++i) {
        new->colors[i] = malloc(sizeof(Color) * height);
    }


    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            movePixel(x, y, new, src);
        }
    }
    freeImage(src);
    return new;
}


