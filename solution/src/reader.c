#include "reader.h"
#include "structs.h"
#include <malloc.h>
#include <stdio.h>

#pragma pack(push, 1)
typedef struct Header{
    unsigned char BM[2];
    uint32_t biSize;
    uint32_t Unused;
    uint32_t biOffset;
    uint32_t sizeHeader;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bits;
    uint32_t compression;
    uint32_t sizeImage;
    uint32_t xPerMeter;
    uint32_t yPerMeter;
    uint32_t clrUsed;
    uint32_t indColor;
} Header;
#pragma pack(pop)

Header newHeader(uint32_t width, uint32_t height, unsigned long padding){
    Header header = {"BM",54 + width * height * 3 + height * padding,0,54,40,width,height,1,24,0,width * height * 3 + height * padding,0,0,0,0};

    return header;
}
enum Status Check(Header header, size_t size){
    if(header.biOffset != 54 || header.sizeHeader != 40 || header.biSize != size)
        return ERROR_SIZE;
    else if(header.BM[0] != 'B' || header.BM[1] != 'M')
        return BM;
    else if(header.bits != 24)
        return BITS;
    else if(header.compression != 0)
        return COMP;
    return OK;
}


enum writeStatus createBMP(FILE* file, Image* image) {
    unsigned long padding = (4 - ((3 * image->width) % 4)) % 4;

    if(file == NULL)
        return WRITE_ERROR;

    struct Header header = newHeader(image->width, image->height, padding);

    fwrite(&header, sizeof(Header), 1, file);

    const int zero = 0;
    for (uint32_t x = 0; x < image->height; ++x) {
        for (int y = 0; y < image->width; ++y) {
            fwrite(&image->colors[x][y], size_color, 1, file);
        }
        fwrite(&zero, padding, 1, file);
    }
    return OKAY;
}


enum Status loadBmp(FILE* Source, Image* New){
    Header headers;

    fseek(Source, 0L, SEEK_END);
    size_t size = ftell(Source);
    rewind(Source);

    fread(&headers, sizeof(Header), 1, Source);

    enum Status status = Check(headers, size);
    if(status != OK)
        return status;

    New->width = headers.width;
    New->height = headers.height;
    New->colors = malloc(sizeof(Color*) * headers.height + 1);

    unsigned int padding = (4- ((3 * headers.width) % 4)) % 4;
    for (uint32_t i = 0; i < headers.height; ++i) {
        New->colors[i] = (Color*)malloc(size_color * headers.width + 1);
        for (int j = 0; j < headers.width; ++j) {
            fread(&New->colors[i][j], size_color, 1, Source);
        }
        fseek(Source, padding, SEEK_CUR);
    }
    return status;
}






