#include "reader.h"
#include "rotator.h"
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>

int main(int argc, char* argv[]) {
    (void) argc;
    FILE* Source = fopen(argv[1], "rb");
    FILE* New = fopen(argv[2], "wb");

    Image* image = malloc(sizeof(Image));

    loadBmp(Source, image);
    Image* new = rotate(image);
    createBMP(New, new);

    fclose(Source);
    fclose(New);



    freeImage(new);
    return 0;

}
