#include "structs.h"
#include <stddef.h>
#include <stdio.h>

enum Status{
    OK = 0,
    BM = 1,
    BITS = 2,
    ERROR_SIZE = 3,
    COMP = 4
};

//enum Status Check(Header header, size_t size);
enum Status loadBmp(FILE* Source, Image* New);

enum writeStatus{
    OKAY,
    WRITE_ERROR
};
enum writeStatus createBMP(FILE* file, Image* image);


