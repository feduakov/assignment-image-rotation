#pragma once


#include <stddef.h>
#include <stdint.h>

#pragma pack(push, 1)
typedef struct Color{
    uint8_t b, g, r;
} Color;
#pragma pack(pop)

static const size_t size_color = sizeof(Color);

typedef struct Image{
    uint32_t width, height;
    Color** colors;
} Image;




